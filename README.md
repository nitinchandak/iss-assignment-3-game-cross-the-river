# ISS-Assignment-3-Game

A short simple game made using python.

Cross The River:
    This is a small game made as a part of our Intro to Software Systems course's assignment.In this game two players compete against each other to reach the opposite banks while collecting maximum points and in least time.
    player's play one at a time.The player who scores 3 points first is declared as the winner.

    All the basic pygame methods are used to develop this game.
    All the basic requirements given in our assignment have been fulfilled.
    Apart from that i have added a home screen,an instruction page,a game replay page,and few extra items like the player moves on its own when in water,extra bonus points,etc.

    
