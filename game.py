import pygame
import random
import math
import configparser
import os

game_count = 0
pygame.init()
screen = pygame.display.set_mode((1040, 930))
running = True
pygame.display.set_caption("Cross The River")


def gamestart():
    global running
    global game_count
    game_play_score = 3

    # importing variables from config file
    configParser = configparser.RawConfigParser()
    configFilePath = os.path.join("./", "config.cfg")
    configParser.read(configFilePath)
    # colors
    Green = configParser.get("info", "Green")
    Black = configParser.get("info", "Black")
    Brown = configParser.get("info", "Brown")
    WaterBlue = configParser.get("info", "WaterBlue")
    # fonts
    message_Font = configParser.get("info", "message_Font")
    message_Font_any_key = configParser.get("info", "message_Font_any_key")
    instruction = configParser.get("info", "instruction")
    instruction_points = configParser.get("info", "instruction_points")
    any_key = configParser.get("info", "any_key")
    turnp2Font = configParser.get("info", "turnp2Font")
    turnp1Font = configParser.get("info", "turnp1Font")
    pause_font = configParser.get("info", "pause_font")
    score_font = configParser.get("info", "score_font")
    score_end_font = configParser.get("info", "score_end_font")
    score_end_font_player = configParser.get("info", "score_end_font_player")
    # converting fonts from string to their respective types
    message_Font = eval(message_Font)
    message_Font_any_key = eval(message_Font_any_key)
    instruction = eval(instruction)
    instruction_points = eval(instruction_points)
    any_key = eval(any_key)
    turnp2Font = eval(turnp2Font)
    turnp1Font = eval(turnp1Font)
    pause_font = eval(pause_font)
    score_font = eval(score_font)
    score_end_font = eval(score_end_font)
    score_end_font_player = eval(score_end_font_player)

    # messages
    screen_Wel_message = configParser.get("info", "screen_Wel_message")
    screen_space_message = configParser.get("info", "screen_space_message")
    turnP2Message = configParser.get("info", "turnP2Message")
    turnP1Message = configParser.get("info", "turnP1Message")
    pause_message = configParser.get("info", "pause_message")
    game_replay_message = configParser.get("info", "game_replay_message")
    game_over_message = configParser.get("info", "game_over_message")
    win_message = configParser.get("info", "win_message")
    # converting from string to their respective types
    # colors
    Green = eval(Green)
    Black = eval(Black)
    Brown = eval(Brown)
    WaterBlue = eval(WaterBlue)
    # messages
    screen_Wel_message = eval(screen_Wel_message)
    screen_space_message = eval(screen_space_message)
    turnP1Message = eval(turnP1Message)
    turnP2Message = eval(turnP2Message)
    pause_message = eval(pause_message)
    game_replay_message = eval(game_replay_message)
    game_over_message = eval(game_over_message)
    win_message = eval(win_message)

    # screens color
    def screen_fill_colors():
        pygame.display.set_caption("Cross the river")
        pygame.draw.rect(screen, Brown, (0, 0, 1040, 70), 0)
        pygame.draw.rect(screen, WaterBlue, (0, 70, 1040, 102), 0)
        pygame.draw.rect(screen, Brown, (0, 172, 1040, 70), 0)
        pygame.draw.rect(screen, WaterBlue, (0, 242, 1040, 102), 0)
        pygame.draw.rect(screen, Brown, (0, 344, 1040, 70), 0)
        pygame.draw.rect(screen, WaterBlue, (0, 414, 1040, 102), 0)
        pygame.draw.rect(screen, Brown, (0, 516, 1040, 70), 0)
        pygame.draw.rect(screen, WaterBlue, (0, 586, 1040, 102), 0)
        pygame.draw.rect(screen, Brown, (0, 688, 1040, 70), 0)
        pygame.draw.rect(screen, WaterBlue, (0, 758, 1040, 102), 0)
        pygame.draw.rect(screen, Brown, (0, 860, 1040, 70), 0)

    # players dimensions
    # player 1
    boy1_image = pygame.image.load('boy1.png')
    boy1_x = 504
    boy1_y = 879
    boy1_changeX = 0
    boy1_changeY = 0
    boy1_change = 0.7
    boy1_reached = False
    boy1_increase = 0.5
    boy1_chance = True
    boy1_score = 0
    boy1_points = 0
    boy1_time = pygame.time.get_ticks()
    boy1_seconds = 0
    boy1_goldcount = 0
    # player 2
    boy2_image = pygame.image.load('boy2.png')
    boy2_x = 504
    boy2_y = 2
    boy2_changeX = 0
    boy2_changeY = 0
    boy2_change = 0
    boy2_reached = False
    boy2_increase = 0.5
    boy2_chance = False
    boy2_score = 0
    boy2_points = 0
    boy2_time = pygame.time.get_ticks()
    boy2_seconds = 0
    boy2_goldcount = 0

    # waves and ships
    waves_image = []
    waves_positionX = []
    waves_positionY = []
    waves_positiondiv2 = 0
    waves_positionndiv2 = 488
    ships_image = []
    ships_positionX = []
    ships_positionY = []
    # assigning values to ships and waves
    for i in range(10):
        waves_image.append(pygame.image.load('flood.png'))
        ships_image.append(pygame.image.load('ship.png'))
        if i % 2 == 0:
            waves_positionX.append(waves_positiondiv2)
            ships_positionX.append(waves_positiondiv2 + 244)
            waves_positiondiv2 += 98
        else:
            waves_positionX.append(waves_positionndiv2)
            ships_positionX.append(waves_positionndiv2 + 244)
            waves_positionndiv2 += 98
        if i == 0 or i == 1:
            waves_positionY.append(89)
            ships_positionY.append(89)
        if i == 2 or i == 3:
            waves_positionY.append(261)
            ships_positionY.append(261)
        if i == 4 or i == 5:
            waves_positionY.append(433)
            ships_positionY.append(433)
        if i == 6 or i == 7:
            waves_positionY.append(605)
            ships_positionY.append(605)
        if i == 8 or i == 9:
            waves_positionY.append(777)
            ships_positionY.append(777)

    # steady obstacles = jungle and volcano and gold
    jungle_image = []
    jungle_positionX = []
    jungle_positionY = []
    volcano_image = []
    volcano_positionX = []
    volcano_positionY = []
    gold_image = [1] * 4
    gold_positionX = [1] * 4
    gold_positionY = [1] * 4

    def gold_positions():
        for gold in range(4):
            gold_image[gold] = pygame.image.load('gold.png')
            gold_positionX[gold] = random.randint(20, 1000)
            if gold == 0:
                gold_positionY[gold] = 174
            if gold == 1:
                gold_positionY[gold] = 346
            if gold == 2:
                gold_positionY[gold] = 518
            if gold == 3:
                gold_positionY[gold] = 690

    gold_positions()
    # assigning values to jungle and volcano
    for i in range(6):

        jungle_image.append(pygame.image.load('jungle.png'))
        jungle_positionX.append(random.randint(20, 1000))
        volcano_image.append(pygame.image.load('volcano.png'))
        volcano_positionX.append(random.randint(20, 1000))
        if i == 0:
            volcano_positionY.append(174)
            jungle_positionY.append(346)
        if i == 1:
            volcano_positionY.append(174)
            jungle_positionY.append(174)
        if i == 2:
            volcano_positionY.append(346)
            jungle_positionY.append(346)
        if i == 3:
            volcano_positionY.append(518)
            jungle_positionY.append(518)
        if i == 4:
            volcano_positionY.append(690)
            jungle_positionY.append(690)
        if i == 5:
            volcano_positionY.append(518)
            jungle_positionY.append(690)

    # functions for showing objects on screen
    def show_waves(x, y, i):
        screen.blit(waves_image[i], (x, y))

    def show_ships(x, y, i):
        screen.blit(ships_image[i], (x, y))

    def show_boy1(x, y):
        screen.blit(boy1_image, (x, y))

    def show_boy2(x, y):
        screen.blit(boy2_image, (x, y))

    def show_jungle_volcano(jx, jy, vx, vy, i):
        screen.blit(volcano_image[i], (vx, vy))
        screen.blit(jungle_image[i], (jx, jy))

    def show_gold(x, y, i):
        screen.blit(gold_image[i], (x, y))

    # function for calculating distance between player and obstacle
    def collision(x1, y1, x2, y2):
        distance = math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2))
        if distance < 60:
            return True
        return False

    welcome_text_screen = True
    instruction_text_screen = True
    final_score_screen = True
    pause_game = False

    # main game screen
    while running:
        if game_count == 0:
            # Welcome screen
            while welcome_text_screen:
                jungle_background = pygame.image.load('jungle_background.jpg')
                screen.blit(jungle_background, (0, 0))
                screen.blit(screen_Wel_message, (250, 440))
                screen.blit(screen_space_message, (330, 540))
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        welcome_text_screen = False
                        running = False
                        final_score_screen = False
                        instruction_text_screen = False
                    if event.type == pygame.KEYUP:
                        welcome_text_screen = False

        if not running:
            break
        # instruction screen
        if game_count == 0:
            while instruction_text_screen:
                screen.fill(Black)
                instruction_message = instruction.render("Instructions :", True, Brown)
                instruction_points_1 = instruction_points.render("1) This game has two players playing one at a time.",
                                                                 True, Green)
                instruction_points_2 = instruction_points.render(
                    "2) Each player has to reach opposite banks but there is a twist.", True, Green)
                instruction_points_3 = instruction_points.render(
                    "3) The path contains hurdles(moving(ships) and steady(jungle and volcanoe's)) which when crossed "
                    "gives the player playing 10 points", True, Green)
                instruction_points_3a = instruction_points.render("for moving hurdles and 5 points for steady hurdles.",
                                                                  True, Green)
                instruction_points_4 = instruction_points.render(
                    "4) The path also contains gold nuggets which when collected gets the player 20 points bonus.",
                    True, Green)
                instruction_points_5 = instruction_points.render(
                    "5) RIGHT,LEFT,TOP,BOTTOM arrow keys and D,A,W,S keys should be used to move FIRST and SECOND  ",
                    True, Green)
                instruction_points_5a = instruction_points.render(
                    "player in RIGHT,LEFT,TOP,BOTTOM direction respectively.",
                    True, Green)
                instruction_points_6 = instruction_points.render(
                    "6) If the points acquired by each player are same the their score depends on the time taken by "
                    "them to", True, Green)
                instruction_points_6a = instruction_points.render("reach opposite bank.", True, Green)
                instruction_points_7 = instruction_points.render("7) The game will be played for 3 rounds.", True,
                                                                 Green)
                instruction_points_8 = instruction_points.render("8) Press SPACE to pause the game.", True, Green)
                any_key_message = any_key.render("Press Any Key To Play The Game", True, Brown)
                screen.blit(instruction_message, (10, 10))
                screen.blit(instruction_points_1, (10, 120))
                screen.blit(instruction_points_2, (10, 160))
                screen.blit(instruction_points_3, (10, 200))
                screen.blit(instruction_points_3a, (35, 230))
                screen.blit(instruction_points_4, (10, 270))
                screen.blit(instruction_points_5, (10, 310))
                screen.blit(instruction_points_5a, (35, 340))
                screen.blit(instruction_points_6, (10, 380))
                screen.blit(instruction_points_6a, (35, 410))
                screen.blit(instruction_points_7, (10, 450))
                screen.blit(instruction_points_8, (10, 490))
                screen.blit(any_key_message, (200, 680))
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        instruction_text_screen = False
                        running = False
                        final_score_screen = False
                    if event.type == pygame.KEYUP:
                        instruction_text_screen = False
                        boy1_time = pygame.time.get_ticks()
        if not running:
            break
        screen_fill_colors()
        for i in range(10):
            waves_positionX[i] = (waves_positionX[i] + 0.2) % 1040
            if boy1_chance:
                ships_positionX[i] = (ships_positionX[i] + boy1_increase) % 1040
            if boy2_chance:
                ships_positionX[i] = (ships_positionX[i] + boy2_increase) % 1040
            show_waves(waves_positionX[i], waves_positionY[i], i)
            show_ships(ships_positionX[i], ships_positionY[i], i)
        for i in range(6):
            show_jungle_volcano(jungle_positionX[i], jungle_positionY[i], volcano_positionX[i], volcano_positionY[i], i)
        for i in range(4):
            show_gold(gold_positionX[i], gold_positionY[i], i)
        # checking for boy 1's collision
        for i in range(10):
            if i < 4:
                is_collision_gold = collision(boy1_x, boy1_y, gold_positionX[i], gold_positionY[i])
                if is_collision_gold:
                    boy1_goldcount += 1
                    gold_positionX[i] = 2040
            if i < 6:
                is_collision_jungle = collision(boy1_x, boy1_y, jungle_positionX[i], jungle_positionY[i])
                is_collision_volcano = collision(boy1_x, boy1_y, volcano_positionX[i], volcano_positionY[i])
            is_collision_ship = collision(boy1_x, boy1_y, ships_positionX[i], ships_positionY[i])
            if is_collision_jungle or is_collision_volcano or is_collision_ship or boy1_y <= 6:
                screen.blit(turnP2Message, (270, 415))
                pygame.display.update()
                pygame.time.wait(1000)
                if boy1_y <= 6:
                    boy1_reached = True
                    boy1_increase += 0.2
                    boy1_points = 160
                else:
                    boy1_reached = False
                boy1_points += (boy1_goldcount * 20)
                boy2_points += (boy2_goldcount * 20)
                boy1_x = 504
                boy1_y = 879
                boy1_change = 0
                boy2_change = 0.7
                boy1_chance = False
                boy2_chance = True
                gold_positions()
                boy2_time = pygame.time.get_ticks()
                boy1_goldcount = 0

        # checking for boy 2's collision and checking score
        for i in range(10):
            if i < 4:
                is_collision_gold2 = collision(boy2_x, boy2_y, gold_positionX[i], gold_positionY[i])
                if is_collision_gold2:
                    boy2_goldcount += 1
                    gold_positionX[i] = 2040
            if i < 6:
                is_collision_jungle2 = collision(boy2_x, boy2_y, jungle_positionX[i], jungle_positionY[i])
                is_collision_volcano2 = collision(boy2_x, boy2_y, volcano_positionX[i], volcano_positionY[i])
            is_collision_ship2 = collision(boy2_x, boy2_y, ships_positionX[i], ships_positionY[i])

            if is_collision_jungle2 or is_collision_volcano2 or is_collision_ship2 or boy2_y >= 856:
                if boy2_y >= 856:
                    boy2_reached = True
                    boy2_increase += 0.2
                    boy2_points = 160
                else:
                    boy2_reached = False
                boy1_points += (boy1_goldcount * 20)
                boy2_points += (boy2_goldcount * 20)
                if boy1_points >= 160 or boy2_points >= 160:
                    if boy1_points > boy2_points:
                        boy1_score += 1
                    if boy2_points > boy1_points:
                        boy2_score += 1
                    if boy1_points == boy2_points:
                        if boy1_seconds > boy2_seconds:
                            boy2_score += 1
                        if boy1_seconds < boy2_seconds:
                            boy1_score += 1
                if boy1_score != game_play_score and boy2_score != game_play_score:
                    screen.blit(turnP1Message, (270, 415))
                    score_message = turnp1Font.render(str(boy1_score) + " - " + str(boy2_score), True, (18, 97, 9))
                    screen.blit(score_message, (440, 515))
                pygame.display.update()
                pygame.time.wait(2000)
                boy2_x = 504
                boy2_y = 2
                boy1_change = 0.7
                boy2_change = 0
                boy1_chance = True
                boy2_chance = False
                boy1_points = 0
                boy2_points = 0
                gold_positions()
                boy1_time = pygame.time.get_ticks()
                boy2_goldcount = 0

        # keys handling movement of players and quitting game and position of players
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    boy1_changeX -= boy1_change
                if event.key == pygame.K_RIGHT:
                    boy1_changeX += boy1_change
                if event.key == pygame.K_DOWN:
                    boy1_changeY += boy1_change
                if event.key == pygame.K_UP:
                    boy1_changeY -= boy1_change
                if event.key == pygame.K_a:
                    boy2_changeX -= boy2_change
                if event.key == pygame.K_d:
                    boy2_changeX += boy2_change
                if event.key == pygame.K_s:
                    boy2_changeY += boy2_change
                if event.key == pygame.K_w:
                    boy2_changeY -= boy2_change
                if event.key == pygame.K_SPACE:
                    pause_game = True
            if event.type == pygame.KEYUP:
                boy1_changeX = 0
                boy1_changeY = 0
                boy2_changeX = 0
                boy2_changeY = 0

        while pause_game:
            screen.blit(pause_message, (7, 365))
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        pause_game = False
                if event.type == pygame.QUIT:
                    pause_game = False
                    running = False
        if not running:
            break

        if 70 <= boy1_y <= 108 or 242 <= boy1_y <= 280 or 414 <= boy1_y <= 452 or 586 <= boy1_y <= 624 or 758 <= boy1_y <= 796:
            boy1_x += 0.5
        if 70 <= boy2_y <= 108 or 242 <= boy2_y <= 280 or 414 <= boy2_y <= 452 or 586 <= boy2_y <= 624 or 758 <= boy2_y <= 796:
            boy2_x += 0.5
        boy1_x += boy1_changeX
        boy1_y += boy1_changeY
        boy2_x += boy2_changeX
        boy2_y += boy2_changeY
        if boy1_x <= 0:
            boy1_x = 0
        if boy1_x >= 976:
            boy1_x = 976
        if boy1_y <= 0:
            boy1_y = 0
        if boy1_y >= 866:
            boy1_y = 866
        if boy2_x <= 0:
            boy2_x = 0
        if boy2_x >= 976:
            boy2_x = 976
        if boy2_y <= 0:
            boy2_y = 0
        if boy2_y >= 866:
            boy2_y = 866
        show_boy1(boy1_x, boy1_y)
        show_boy2(boy2_x, boy2_y)

        # display score of both players and time

        # points of boy1
        # if 675 <= boy1_y <= 930:
        #     boy1_points = 0
        if 688 <= boy1_y <= 694:
            boy1_points = 20
        if 586 <= boy1_y <= 620:
            boy1_points = 35
        if 516 <= boy1_y <= 522:
            boy1_points = 55
        if 414 <= boy1_y <= 452:
            boy1_points = 70
        if 344 <= boy1_y <= 350:
            boy1_points = 90
        if 242 <= boy1_y <= 280:
            boy1_points = 105
        if 172 <= boy1_y <= 178:
            boy1_points = 125
        if 70 <= boy1_y <= 108:
            boy1_points = 140
        if 0 <= boy1_y <= 6:
            boy1_points = 160

        # points of boy2
        if 860 <= boy2_y <= 866:
            boy2_points = 160
        if 758 <= boy2_y <= 796:
            boy2_points = 140
        if 688 <= boy2_y <= 674:
            boy2_points = 125
        if 586 <= boy2_y <= 620:
            boy2_points = 105
        if 516 <= boy2_y <= 522:
            boy2_points = 90
        if 414 <= boy2_y <= 452:
            boy2_points = 70
        if 344 <= boy2_y <= 350:
            boy2_points = 55
        if 242 <= boy2_y <= 280:
            boy2_points = 35
        if 172 <= boy2_y <= 178:
            boy2_points = 20
        # if boy2_y < 172:
        #     boy2_points = 0
        # incrementing playtime for the player playing
        if boy1_chance:
            boy1_seconds = (pygame.time.get_ticks() - boy1_time) / 1000
        if boy2_chance:
            boy2_seconds = (pygame.time.get_ticks() - boy2_time) / 1000
        # showing points.time,player details
        boy1_message = score_font.render("Time: " + str(boy1_seconds), True, Green)
        boy2_message = score_font.render("Time: " + str(boy2_seconds), True, Green)
        player1_message = score_font.render("Player 1", True, Green)
        player2_message = score_font.render("Player 2", True, Green)
        points1_message = score_font.render("score: " + str(boy1_points + (boy1_goldcount * 20)), True, Green)
        points2_message = score_font.render("score: " + str(boy2_points + (boy2_goldcount * 20)), True, Green)
        screen.blit(player1_message, (0, 0))
        screen.blit(player2_message, (920, 0))
        screen.blit(points1_message, (0, 20))
        screen.blit(points2_message, (920, 20))
        screen.blit(boy1_message, (0, 40))
        screen.blit(boy2_message, (920, 40))
        # screen after game has ended
        while final_score_screen and (boy1_score == game_play_score or boy2_score == game_play_score):
            screen.fill(Black)
            if boy1_score < boy2_score:
                win_message = score_end_font_player.render("Player 2 Won", True, Green)
            score = score_end_font.render("Score:", True, Green)
            player1_score = score_end_font_player.render("Player 1: " + str(boy1_score), True, Green)
            player2_score = score_end_font_player.render("Player 2: " + str(boy2_score), True, Green)
            screen.blit(game_over_message, (100, 100))
            screen.blit(win_message, (340, 340))
            screen.blit(score, (340, 440))
            screen.blit(player1_score, (370, 640))
            screen.blit(player2_score, (370, 740))
            screen.blit(game_replay_message, (390, 840))
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    final_score_screen = False
                if event.type == pygame.KEYDOWN:
                    final_score_screen = False
                    game_count += 1
                    gamestart()

        pygame.display.update()


if running:
    gamestart()
